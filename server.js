var app = require('express')();
var Raven = require('raven');
var count = 0;
const readline = require('readline');
var dsn = '';


const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function startConfig() {

  app.get('/', function mainHandler(req, res) {
      throw new Error('Test error!');
  });

  app.get('/ping', function mainHandler(req, res) {

      res.json({"message":"pong"})
      count ++;
  });
  app.get('/count', function mainHandler(req, res) {
      res.json({"pingCount": count})
  });

  app.use(function onError(err, req, res, next) {
      res.statusCode = 500;
      res.end(res.sentry + '\n');
      
  });

  app.listen(3000,function(){
    console.log('Enjoy');
    console.log('Listen on port 3000')
    process.on('SIGINT', function() {
        throw new Error('Application Killed');
        
    });
  });
};

 startConfig();